import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Media, MediaObject } from '@ionic-native/media';
/**
 * Generated class for the PlaymusikPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-playmusik',
  templateUrl: 'playmusik.html',
})
export class PlaymusikPage {
	play :any;
	private songMedia : MediaObject = null;
	private paused = false;
  constructor(private media: Media,public navCtrl: NavController, public navParams: NavParams) {
  	this.play = this.navParams.get('data');
  	console.log(this.play);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaymusikPage');
  }

  ionViewWillLeave(){
  	this.stopMusic();
  }

  playMusic(){
  	console.log('play');
  	if (this.songMedia === null) {
	  	this.songMedia = this.media.create(this.play["music_url"]);
  		this.songMedia.play();
  	} else {
  		if (this.paused === true) {
  			this.songMedia.play();
  			this.paused = false;
  		}
  	}
  }

  pauseMusic(){
  	console.log('pause');
  	if (this.songMedia !== null) {
  		this.songMedia.pause();
  		this.paused = true;
  	}
  }

  stopMusic(){
  	console.log('stop');
  	if (this.songMedia !== null) {
	  	this.songMedia.stop();
	  	this.songMedia.release();
  		this.songMedia = null;
  	}
  }

}
