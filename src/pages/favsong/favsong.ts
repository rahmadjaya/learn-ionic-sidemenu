import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { MusikProvider } from '../../providers/musik/musik';
/**
 * Generated class for the FavsongPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-favsong',
  templateUrl: 'favsong.html',
})
export class FavsongPage {
	FavSongs : any;
  constructor(private musikProvider : MusikProvider,public navCtrl: NavController, public navParams: NavParams) {
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavsongPage');
    this.FavSongs = this.musikProvider.getFav();
    console.log(this.FavSongs);
  }

}
