import { Component } from '@angular/core';
import { NavController, LoadingController, ActionSheetController } from 'ionic-angular';
import { MusikProvider } from '../../providers/musik/musik';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PlaymusikPage } from '../playmusik/playmusik';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public allMusik =[];
  constructor(private socialShare : SocialSharing ,private actionSheetCtrl: ActionSheetController ,private loadingCtrl : LoadingController,private musikProvider: MusikProvider ,public navCtrl: NavController) {

  }

  ionViewDidLoad(){
	//loading data  		
  	let allMusikLoading = this.loadingCtrl.create({
  		content: "Mohon tunggu sebentar"
  	});
  	allMusikLoading.present();
  	this.musikProvider.getMusik()
  		.subscribe(res => {
  			allMusikLoading.dismiss();
  			this.allMusik = res;
  			console.log(this.allMusik);
  		});
  }

  addOnemusik(refresher){
  	this.musikProvider.getMusik()
  		.subscribe(oneSong=> {
  			this.allMusik.unshift(oneSong[0]);
  			refresher.complete();
  		});
  }

  shareSong(data){
  	///action pop up share
  	let shareSongAction = this.actionSheetCtrl.create({
  		title : "Share your song",
  		buttons: [
  			{
  				text : "Share on Facebook",
  				icon : "logo-facebook",
          handler: ()=>{
            this.socialShare.shareViaFacebook(data.name, data.image, data.music_url);
          }
  			},
  			{
  				text : "Share on Twitter",
  				icon : "logo-twitter",
          handler : ()=>{
            this.socialShare.shareViaTwitter(data.name, data.image, data.music_url);
          }
  			},
  			{
  				text : "Share",
  				icon : "md-share",
          handler: ()=>{
            this.socialShare.share(data.name, "", data.image, data.music_url);
          }
  			},
  			{
  				text: "Cancel",
  				role : "cancel"
  			}
  		]
  	});
  	shareSongAction.present();
  }

  listenSong(data){
    this.navCtrl.push(PlaymusikPage, {data : data});
  }

  addFav(data){
    this.musikProvider.addToFav(data);
    console.log('berhasil di add fav');
  }
}
