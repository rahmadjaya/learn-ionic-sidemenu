import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the MusikProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MusikProvider {
  public favSongs =[];

  constructor(public http: Http) {
    console.log('Hello MusikProvider Provider');
  }

  getMusik(){
  return this.http.get('/assets/data.json')
  		.map(res=> res.json())
  }

  getFav(){
    return this.favSongs;
  }

  addToFav(song){
    let isSongAdd = this.favSongs.findIndex((favSong)=>{
      return song.id === favSong.id
    });
    if (isSongAdd === -1) {
      this.favSongs.push(song);
    }
  }
}
