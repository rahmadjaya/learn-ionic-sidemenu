import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { PlaymusikPage } from '../pages/playmusik/playmusik';
import { FavsongPage } from '../pages/favsong/favsong';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Media} from '@ionic-native/media';
import { MusikProvider } from '../providers/musik/musik';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    PlaymusikPage,
    FavsongPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    PlaymusikPage,
    FavsongPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    Media,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MusikProvider
  ]
})
export class AppModule {}
